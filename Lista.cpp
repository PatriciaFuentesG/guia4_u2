#include <iostream>
using namespace std;

#include "Lista.h"
#include <fstream>


Lista::Lista() {}
//Este menu se encarga de llamar las funciones segun las opcion escogida por el usuario
void Lista::menu(int opc, int numero){
    
    Nodo *locale = this->raiz;

    if(opc == 1){
    Nodo *arbol;
    
        arbol = new Nodo;
        arbol->der = NULL;
        arbol->izq = NULL;
        arbol->numero = numero;
        insertar(arbol, numero);
    }else if(opc == 2){
       
        eliminar(locale,numero);    

    }else if(opc == 3){
       
        eliminar(locale,numero);  
        if(numero == 1){
            imprimir_preorden(locale);
        }if(numero == 2){
            imprimir_inorden(locale);
        }else{
            imprimir_posorden(locale);
        }
    }else if(opc == 4){
        
        grafo(locale);
    }else if (opc == 5){

        modificar(locale, numero);

    }
}

void Lista::insertar(Nodo *Nodo, int numero){
    if(raiz == NULL){
       this->raiz  = Nodo; 
       
    }else if(numero< Nodo->numero){
        if (Nodo->izq == NULL){
            Nodo->izq = Nodo;
        }else{
            insertar(Nodo->izq,numero);
        }
    }else if(numero > Nodo->numero){
         if(Nodo->der== NULL){
             Nodo->der = Nodo;
         }else{
             insertar(Nodo->der, numero);
         }

    }else{//esto es para evitar la repeticion de numeros
       cout<< "Este numeros ya se encuentra en el arbol"<<endl; 
    }
}


void Lista::imprimir_inorden (Nodo *nodo) {
    if(this->raiz != NULL){
        imprimir_inorden(nodo->izq);
        cout<<"Valor: "<<nodo->numero<<endl;
        imprimir_inorden(nodo->der);
    }
    
}
void Lista::imprimir_preorden (Nodo *nodo) {
    if(this->raiz != NULL){
        cout<<"Valor: "<<nodo->numero<<endl;
        imprimir_preorden(nodo->izq);
        imprimir_preorden(nodo->der);

    }  
}
void Lista::imprimir_posorden (Nodo *nodo) {
    if(this->raiz != NULL){
        imprimir_posorden(nodo->izq);
        imprimir_posorden(nodo->der);
        if(this->raiz != NULL){
        cout<<"Valor: "<<nodo->numero<<endl;     
       }
    }
    
}


void Lista::modificar(Nodo *nodo,int numero){
    if(numero < nodo->numero){
        if(nodo->izq == NULL){
            cout << "La informacion no se encuentra en el arbol" << endl;
        }else{
            modificar(nodo->izq,numero);
        }
    }else{
        if(numero> nodo->numero){
            if(nodo->der == NULL){
                cout << "La informacion no se encuentra en el arbol" << endl;
            }else{
                modificar(nodo->der,numero);
            }
        }else{
            cout << "La informacion esta en el arbol, escriba el valor que lo reemplazara:  " << endl;
            int dato;
            cin >> dato;
            nodo->numero = dato; 
        }
    }
}


void Lista::eliminar(Nodo *nodo,int numero){
    
    if( nodo != NULL){
        if(numero < nodo->numero){
            eliminar(nodo->izq,numero);
        }else{
            if(numero> nodo->numero){
                eliminar(nodo->der,numero);
            }else{
                Nodo *hoja = nodo;
                if(hoja->der == NULL){
                    nodo = hoja->izq;
                }else{
                    if(nodo->izq == NULL){
                        nodo = hoja->der;

                    }else{
                        Nodo *aux = nodo->izq;
                        bool boleano = false;
                        Nodo *aux1;
                        while(aux->der != NULL){
                            aux1 = aux;
                            aux = aux->der;
                            boleano = true;
                        }
                        nodo->numero = aux->numero;
                        hoja = aux;
                        if(boleano == true){
                            aux1->der = aux->izq;
                        }else{
                            nodo->izq = aux->izq;
                        }
                    }
                }
            }
        }
    }
}



void Lista::grafo (Nodo *Nodo) {
  
    ofstream fp;
            
            /* abre archivo */
    fp.open ("grafo.txt");

    fp << "digraph G {" << endl;
    fp << "node [style=filled fillcolor=yellow];" << endl;
                
    recorrer(Nodo, fp);

    fp << "}" << endl;

            /* cierra archivo */
    fp.close();
                    
            /* genera el grafo */
    system("dot -Tpng -ografo.png grafo.txt &");
            
            /* visualiza el grafo */
    system("eog grafo.png &");
}
void Lista::recorrer(Nodo *p, ofstream &fp){

    string cadena = "\0";
              
    if (p != NULL) {
        if (p->izq != NULL) {
            fp <<  p->numero << "->" << p->izq->numero << ";" << endl;
        } else {      
            cadena = p->numero + "i";
            fp <<  cadena << " [shape=point];" << endl;
            fp << p->numero << "->" << cadena << ";" << endl;
        }
                    
        if (p->der != NULL) { 
             fp << p->numero << "->" << p->der->numero << ";" << endl;
        } else {
             cadena = p->numero + "d";
             fp <<  cadena << " [shape=point];" << endl;
             fp << p->numero << "->" << cadena << ";" << endl;
         }

             recorrer(p->izq, fp);
             recorrer(p->der, fp); 
            
        }

}