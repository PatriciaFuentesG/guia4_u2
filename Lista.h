#ifndef LISTA_H
#define LISTA_H

#include <iostream>
using namespace std;




typedef struct _Nodo {
    
    int numero = 0;
    struct _Nodo *izq;
    struct _Nodo *der;
} Nodo;

class Lista {
    private:
        Nodo *raiz = NULL;

    public:
       
        Lista();
        
        void menu(int opc, int numero);

        void recorrer(Nodo *p, ofstream &fp);

        void insertar(Nodo *nodo,int numero);

        void imprimir_preorden(Nodo *nodo);

        void imprimir_inorden(Nodo *nodo);

        void imprimir_posorden(Nodo *nodo);

        void modificar(Nodo *nodo,int numero);

        void eliminar(Nodo *nodo,int numero);

        void grafo(Nodo *Nodo);

     
};
#endif
