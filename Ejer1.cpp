
#include <iostream>

using namespace std;
#include "Lista.h"


 int menu(){
     string opc;

     cout << "***********MENU**********" << endl;
     cout << "Agregar               [1]" << endl;
     cout << "Eliminar              [2]" << endl;
     cout << "Imprimir              [3]" << endl;
     cout << "Grafo                 [4]" << endl;
     cout << "Modificar             [5]" << endl;
     cout << "Salir                 [0]" << endl;
     cout << "*************************" << endl;
     cout <<  "Opcion:";
     cin >> opc;
  
  return stoi(opc);
 }

 int escoger_impresion(){

     string opc;

     cout << "***********MENU**********" << endl;
     cout << "Preorden               [1]" << endl;
     cout << "Inorden                [2]" << endl;
     cout << "Posorden               [3]" << endl;
     cout << "*************************" << endl;
     cout <<  "Opcion:";
     cin >> opc;

   return stoi(opc);

 }


int main (void) {
    
    Lista *lista = new Lista();
    int opc = -1;
    int dato = 0;
    int imprimir = 0;
    
    while (opc != 0){
   
        opc = menu();

         if(opc == 1){

             cout << "Valor: ";
             cin >> dato;
             lista->menu(opc,dato);
            
         }else if(opc == 2){

             cout << "Valor: ";
             cin >> dato;
             lista->menu(opc,dato);
             

         }else if(opc == 3){

             imprimir = escoger_impresion();

             if(imprimir == 1){
                 lista->menu(opc,imprimir);
             }else if(imprimir == 2){
                  lista-> menu(opc,imprimir);  
             }else{
                 lista->menu(opc,imprimir);
             }
         }else if (opc == 4){
             lista->menu(opc,0);
         }else if (opc == 5){
             cout << "Valor a buscar y reemplazar: ";
             cin >> dato;
             lista->menu(opc,dato);
         }
                

    }  
    
  
  
   delete lista;

    return 0;
}