prefix=/usr/local
CC = g++

CFLAGS = -g -Wall 
SRC = Ejer1.cpp Lista.cpp 
OBJ = Ejer1.o Lista.o
APP = Ejer1

all: $(OBJ)
	$(CC) $(CFLAGS)-o $(APP) $(OBJ) 

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)

